$(document).ready(function(){

/*PAGE SCROLL TO ID ==================================
=====================================================*/
    $(".section2__buuton, .bootom_panel a, .footer__list li a").mPageScroll2id({
        offset: 0,
        scrollSpeed : 750
    });

/*MAGNIFIC POPUP =====================================
=====================================================*/
$('.popup-with-move-anim').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,
    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom'
  });

/*CALL COUNTER PRICE =================================
=====================================================*/
    $('.counter').counterUp({
      delay: 60, // the delay time in ms
      time: 2000 // the speed time in ms
    });


/*TOOLTYPES ==========================================
=====================================================*/
    $('[data-toggle="tooltip"]').tooltip();


/*TOGGLE TABS ========================================
=====================================================*/
var filter = $('.active__filter').attr('data-filter');
  $('.section5__content_box').hide();
  $('#tabs_container').find('#' + filter).show();

$('.toggle__button_tab').click(function() {

  
  $('.toggle__button_tab').removeClass('active__filter');
  $(this).addClass('active__filter');

  var filter = $('.active__filter').attr('data-filter');
  $('.section5__content_box').hide(500);
  $('#tabs_container').find('#' + filter).show(500);
});


/* AJAX SEND-EMAIL ===================================
=====================================================*/
    $("form").submit(function() { //Change
      var th = $(this);
      $.ajax({
        type: "POST",
        url: "mail.php", //Change
        data: th.serialize()
      }).done(function() {
        window.location.href = "/sent-ok.html";
        setTimeout(function() {
          th.trigger("reset");
        }, 100);
      });
      return false;
    });
});


