<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Выбор способа оплаты</title>
        <meta charset="utf-8" />
        <link rel="shortcut icon" href="https://152622.selcdn.ru/ah-files/favicon.ico" type="image/x-icon">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://152622.selcdn.ru/ah-files/css/bootstrap.css">
        <link rel="stylesheet" href="https://152622.selcdn.ru/ah-files/css/main.css"> 
    </head>
    <body>
        <section id="pay-page">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 pay-logo" style="text-align: center;">
                            <img src="https://152622.selcdn.ru/ah-files/img/svg/logo.svg" alt="">
                        </div>
                    <div class="col-12 col-lg-10 row justify-content-center">
                        <div class="col-12 pay-title">Выберите способ оплаты:</div>
                       <!--  <div class="col-6 wm">
                           <a href="pay/webmoney">WebMoney</a>
                           <span class="desc-sys">Оплата электронными деньгами WebMoney</span>
                           <span>Стандартаная комиссия WebMoney 0.8%</span>
                       </div> -->
                        <div class="col-12 other-pay">
                            <a href="pay/unitpay">Другие способы оплаты</a>
                            <span class="desc-sys">Яндекс Деньги, банковская карта и др.</span>
                            <span>Возможно взимание дополнительной комиссии платежными системами</span>
                        </div>
                    </div>
                </div>
            </div> 
        </section>
    </body>
</html>