<?php session_start(); 
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Оплата WebMoney</title>
	<style>
		body{text-align: center;font-family: 'Roboto', sans-serif;position: relative;}
		h2{color: #2e7d32; font-size: 26px;font-weight: bold;}
		.wrap{display: inline-block; width: auto;padding: 20px;background-color: rgb(238, 238, 238);position: relative;left: 0;right: 0;margin: 0 auto;}
		.header-block {font-size: 28px; font-weight: normal;margin-bottom: 40px;text-transform: uppercase;}
		.pay-form{text-align: center;max-width: 300px;margin: 0 auto;}
		#email {display: inline-block;margin-bottom: 20px;font-size: 18px; padding: 5px 10px;}
		.subm {display: none;}
		.group-btn {margin-top: 10px;}
		.button {background-color: #008cba; padding: 5px 10px;border: 3px solid transparent;color: #fff;font-weight: bold;transition: .3s;margin: 5px 0;display: inline-block;font-size: 14px; min-width: 270px;}
		.button:hover {border-color: #1b79b8;transition: .3s;cursor: pointer;}
		.button:active {background-color: #1b79b8;border-color: #1b79b8;}
		.info-head, .repeat {font-size: 12px;margin-top: 20px;}
		.info-list {list-style: none;font-size: 12px;padding-left: 0;margin: 0;}
		.repeat {margin-bottom: 10px;}
		.info-list a {color: #008cba;}
		.info-list a:hover {text-decoration: none;}
	</style>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body> 
	<div class="wrap">
		<div class="header-block">Двухдневный интенсив по монетизации</div>
		<h2>Оплата через WebMoney</h2>
		<div class="repeat">Введите E-mail, который Вы использовали при оформлении заказа</div>
		<?php
		echo '<form class="pay-form" action="https://merchant.webmoney.ru/lmi/payment.asp"; method="POST">
			 	<input type="hidden" id="amount" name="LMI_PAYMENT_AMOUNT" value=""  >
			 	<input type="hidden" id="desc" maxlength="30" name="LMI_PAYMENT_DESC" value="" >
			 	<input type="hidden" name="LMI_PAYEE_PURSE" value="R443501580263">
			 	<input type="email" id="email" name="ID_USER" placeholder="Ваш e-mail" required>
			 	<div class="group-btn">
				 	<div class="button">Оплатить тариф "Эконом" - <span>7900</span> ₽</div>
				 	<div class="button">Оплатить тариф "Стандарт" - <span>14900</span> ₽</div>
				 	<div class="button">Оплатить тариф "ВИП" - <span>69900</span> ₽</div>
			 	</div>
				<input class="subm" type="submit" value="Оплатить">
			</form>';
		?>
		<div class="info-head">Если есть вопросы, напишите нам через:</div>
		<ul class="info-list">
			<li><a href="https://tglink.me/manager_adhelper_bot" target="_blank">Telegram</a></li>
			<li><a href="https://join.skype.com/bot/d55e7a23-dbee-4a16-bfb8-f85208ec1088" target="_blank">Skype</a></li>
			<li><a href="https://vk.com/im?sel=-141353567" target="_blank">ВКонтакте</a></li>
		</ul>
	</div>
	
	
</body>
<script>
	$(document).ready(function(){
		h = $(window).height() / 2 - $('.wrap').outerHeight() / 2;
		$('.wrap').css('top', Math.round(h) + 'px');
		$(window).keydown(function(event){
		    if(event.keyCode == 13) {
		      	event.preventDefault();
		      	alert("Выберите нужный тариф")
		      	return false;
		    }
	  	});
	})

	document.getElementById("email").onkeyup = function() {
	    this.value = this.value.replace(/[^a-z0-9_\-\.\@\!\#\$\%\&]+/i, '');
	};

	var valid = false;
        $('#email').blur(function() {
            if($(this).val() != '') {
                var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                if(!pattern.test($(this).val())){
                    $(this).css({'color' : '#ff0000'});
                    alert('Введите корректный E-mail');
                }
                else {
                     $(this).css({'color' : '#000'});
                     valid = true;
                }
            } else {
                $(this).css({'color' : '1px solid #ff0000'});
                alert('Поле email не должно быть пустым');
            }
        });

	$('.button').click(function() {
		if (valid == true) {
			$('#desc').val($('#email').val() + ' - Bizon365');
			$('#amount').val($('span', this).text());
			$('.subm').click();
		}
	})
</script>
</html>
