<?php session_start(); 
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Оплата WebMoney</title>
	<style>
		body{text-align: center;}
		h2{color: #3b99d8;}
		.wrap{display: inline-block;margin-top: 20vh;border: 2px solid #3b99d8;padding: 10px 30px 30px;}
		.pay-form{text-align: center;max-width: 250px;margin: 0 auto;}
		#amount{display: inline-block;margin-bottom: 20px;}
		#url {display: inline-block;margin-bottom: 20px;}
		.pay-form input[type=submit] {background-color: #3b99d8; padding: 5px 10px;border: 3px solid transparent;color: #fff;font-weight: bold;transition: .3s;margin: 0 20px;}
		.pay-form input[type=submit]:hover {border-color: #1b79b8;transition: .3s;cursor: pointer;}
		.pay-form input[type=submit]:active {background-color: #1b79b8;border-color: #1b79b8;}
        .info-head {font-size: 12px;margin-top: 20px;}
	</style>
</head>
<body> 
	<div class="wrap">
		<h2>Оплата через WebMoney</h2>
		<?php
		echo '<form class="pay-form" action="https://merchant.webmoney.ru/lmi/payment.asp"; method="POST">
			 	<input type="text" id="amount" name="LMI_PAYMENT_AMOUNT" value="15000" placeholder="Сумма к опл" required>
			 	<input type="text" id="url" placeholder="Адрес Вашего сайта" maxlength="30" name="LMI_PAYMENT_DESC" value="" required>
			 	<input type="hidden" name="LMI_PAYEE_PURSE" value="R443501580263">
			 	<input type="hidden" name="ID_USER" value="'. $_COOKIE['PHPSESSID'] .'">
				<input type="submit" value="Оплатить">
			</form>
            <div class="info-head" style="color: red">Перед проведением платежа отключите все блокировщики рекламы</div>';
		?>
	</div>
</body>
<script>
	document.getElementById("url").onkeyup = function() {
	    this.value = this.value.replace(/[^a-z0-9_\-\.]+/i, '');
	};

	document.getElementById("amount").onkeyup = function() {
	    this.value = this.value.replace(/[^0-9]+/i, '');
	    this.value = Math.round(this.value, -2);
	};
	// document.getElementById("amount").value = parseFloat(this.value).toFixed(2);
</script>
</html>
