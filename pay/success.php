<!DOCTYPE html>
<html>
    <head>
        <title>Платёж осуществлен</title>
        <meta charset="utf-8" />
        <link rel="shortcut icon" href="https://152622.selcdn.ru/ah-files/favicon.ico" type="image/x-icon">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://152622.selcdn.ru/ah-files/css/bootstrap.css">
        <link rel="stylesheet" href="https://152622.selcdn.ru/ah-files/css/main.css"> 
    </head>
    <body>
        <section id="pay-success-page">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-10 row">
                        <div class="col-12 pay-success-title">Успешная оплата</div>
                            <span>Платеж был выполнен</span>
                            <a href="/">Вернуться на главную оплаты</a>
                        </div>
                    </div>
                </div>
            </div> 
        </section>
    </body>
</html>